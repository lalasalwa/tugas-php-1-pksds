<?php
    require_once("ANIMAL.PHP");
    require_once("frog.php");
    require_once("ape.php");
    $sheep = new Animal("shaun");
    echo "Name = " . $sheep->name . "<br>"; 
    echo "Legs = " . $sheep->legs . "<br>"; 
    echo "Cold Blooded = " . $sheep->cold_blooded . "<br>" . "<br>"; 

    $sungokong = new Ape("kera sakti");
    echo "Name = " . $sungokong->name . "<br>"; 
    echo "Legs = " . $sungokong->legs . "<br>"; 
    echo "Cold Blooded = " . $sungokong->cold_blooded . "<br>"; 
    echo "Yell = ";
    echo $sungokong->Yell() . "<br>". "<br>";

    $kodok = new Frog("buduk");
    echo "Name = " . $kodok->name . "<br>"; 
    echo "Legs = " . $kodok->legs . "<br>"; 
    echo "Cold Blooded = " . $kodok->cold_blooded . "<br>"; 
    echo "Yell = ";
    echo $kodok->Yell() . "<br>". "<br>";