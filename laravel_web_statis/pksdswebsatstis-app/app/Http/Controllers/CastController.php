<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\cast;

class CastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cast = cast::all();
        return view('index', compact('cast'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cast = cast::get();
        return view('create', ['cast' => $cast]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request -> validate([
            'name'=> 'required',
            'umur'=> 'required',
            'bio'=> 'required',

        ]);

        $cast = new cast;

        $cast -> name = $request ->name;
        $cast -> umur = $request ->umur;
        $cast -> bio = $request ->bio;

        $cast-> save();

        return redirect('/cast');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $cast = cast::find($id);
        return view('show', compact('cast'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
            $cast = cast::find($id);
            $cast = cast::get();
            return view('edit', compact('cast'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $cast = cast::find($id);
        $cast-> name = $request->name;
        $cast -> umur = $request ->umur;
        $cast-> bio = $request->bio;

        $cast->update();
        return redirect('/cast');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cast = cast::find($id);
        $cast->delete();
        return redirect('/cast');
    }
}
