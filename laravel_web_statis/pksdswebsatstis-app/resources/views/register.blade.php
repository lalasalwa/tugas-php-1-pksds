@extends('layout.master')

@section('judul')
Buat Account baru
@endsection

@section('biodata')
<form action="/isibio", method="post">
    @csrf
        <h2>Buat Account Baru</h2>
    <h4>Sign Up Form</h4>
    <label>First name :</label><br>
    <input type="text" name="name"><br><br>
    <label>Last name :</label>
    <br>
    <input type="text" name="name"><br><br>
    <label>Gender :</label><br>
    <input type="radio" name="Gender">Male<br>
    <input type="radio" name="Gander">Female
    <br>
    <br>
    <label For="Nationality">Nationality :</label><br>
    <select id="Nationality" name="Nationality">
        <option value="Indonesia">Indonesia</option>
        <option value="America">America</option>
        <option value="Inggris">Inggris</option>
    </select>
    <br> <br>
    <label>Language Spoken</label><br>
    <input type="checkbox" name="Language Spoken">Bahasa Indonesia<br>
    <input type="checkbox" name="Language Spoken">English<br>
    <input type="checkbox" name="Language Spoken">Other<br>
    <br> <br>
    <label>Bio</label> <br> <br>
    <textarea name="Bio" rows="10" cols="30"></textarea><br>
    <input type="submit" value="Sign Up">
</form>
@endsection