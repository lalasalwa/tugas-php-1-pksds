 @extends('layout.master')

 @section ('judul')
Media Online
@endsection

@section('biodata')
    <h2>Sosial Media Developer</h2>
    <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
    <h3>Benefit Join di Media Online</h3>
    <ul>
        <li>Mendapatkan motivasi dari sesama developer</li>
        <li>Sharing knowlenge</li>
        <li>Dibuat oleh calon web developer terbaik</li>
    </ul>
    <h3>Cara Bergabung ke Media Online</h3>
    <ol>
        <li>Mengunjungi website ini</li>
        <li> mendaftarkan di
            <a href="/register">Form Sign Up</a>
        </li>
        <li>Selesai</li>
    </ol>
@endsection
