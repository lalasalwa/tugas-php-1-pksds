@extends('layout.master')

@section('judul')
Halaman List Cast
@endsection

@section('biodata')

<a href="/cast/create" class="btn btn-primary btn-sm">List Cast</a>
<div class="row">
    @forelse ($cast as $item)
        <div class="col-4">
            <div class="card">
                <div class="card-body">
                    <h3>{{$item -> name}}</h3>
                    <p class="card-text">{{ $item -> bio}}</p>
                    <a href="/cast/{{$item->id}}" class="btn btn-primary btn-block btn-sm">List Cast</a>
                    <div class="row my-2">
                        <div class="col">
                            <a href="/cast/{{$item->id}}/edit" class="btn btn-primary btn-block btn-sm">edit</a>
                        </div>
                        <div>
                            <form action="/cast/{{$item->id}}" method="cast">
                                @csrf
                                @method('delete')
                                <input type="submit" class="btn btn-danger btn-block btn-sm" value="Delete">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @empty
    <h2>Tidak Ada Postingan</h2>
    @endforelse

</div>

@endsection