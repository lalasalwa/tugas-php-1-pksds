@extends('layout.master')

@section('judul')
Detail Cast
@endsection

@section('biodata')

    <div class="card">
        <div class="card-body">
            <h3>{{$cast->name}}</h3>
            <p class="card-text">{{$cast -> umur}}</p>
            <p class="card-text">{{$cast -> bio}}</p>
            <a href="/cast" class="btn btn-primary btn-block btn-sm">kembali</a>
        </div>
    </div>


@endsection