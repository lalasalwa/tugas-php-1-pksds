Soal 1 Membuat Database
CREATE DATABASE myshop;

Soal 2 Membuat Table di Dalam Database
user
CREATE TABLE user( id int PRIMARY KEY AUTO_INCREMENT, name varchar(255), email varchar(255), password varchar(255) );

categories
CREATE TABLE categories ( id int PRIMARY KEY AUTO_INCREMENT, name varchar(255) );

items
CREATE TABLE item ( name varchar(255), description varchar(255), price int, stock int, categories_id int, FOREIGN KEY (categories_id) REFERENCES categories(id) );

Soal 3 Memasukkan Data pada Table
user
INSERT INTO user (name, email, password) VALUES ('John Doe', 'john@doe.com', 'john123'), ('John Doe', 'Johe"gmail.com', 'jenita123');

Categories
INSERT INTO categories (name) VALUES ('gadget'), ('cloth'), ('men'), ('women'), ('branded');

items
INSERT INTO item (name, description, price, stock, categories_id) VALUES ('Sumsang b50', 'hape keren dari merek sumsang', 4000000, 100, 1), ('Uniklooh', 'baju keren dari brand ternama', 500000, 50, 2), ('IMHO Watch', 'jam tangan anak yang jujur banget', 2000000, 10, 1);